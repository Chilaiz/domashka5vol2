mass = [{"name": "Vasya", "age": 23}, {"name": "Petya", "age": 24}, {"name": "Slavik", "age": 25}, {"name": "Tanya", "age": 16  }]

def filter(arr):
    newMass = []
    for i in arr:
        if len(i) == 2:
            newMass.append(i)
    return newMass



filteredMass = filter(mass)


def addElement(arr):
    for i in arr:
        i["live"] = "In Bishkek"
    return arr


newMass = addElement(filteredMass)


def changed(arr):
    for i in arr:
        i["age"] += 1
    return arr


changedMass = changed(newMass)


def printed(arr):
    for i in arr:
        print(i)


printed(changedMass)
